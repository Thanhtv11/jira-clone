import { closeModal } from "../../redux/layoutSlice/layoutSlice";
import { projectService } from "../../services/project/project.service";
import { openNotification } from "../openNotification/openNotification";
import { getAllProject } from "./getAllProject";

export const createProject = (projectInfo) => {
  return (dispatch) => {
    projectService
      .postProjectCreateProject(projectInfo)
      .then((success) => {
        console.log("success: ", success);
        dispatch(getAllProject());
        openNotification({
          type: "success",
          message: "Create successfully",
        });
        setTimeout(() => {
          dispatch(closeModal());
        }, 300);
      })

      .catch((error) => {
        openNotification({
          type: "error",
          message: error.response.data.content,
        });
      });
  };
};
