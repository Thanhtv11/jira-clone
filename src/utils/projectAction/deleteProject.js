import { projectService } from "../../services/project/project.service";
import { openNotification } from "../openNotification/openNotification";
import { getAllProject } from "./getAllProject";

export const deleteProject = (id) => {
  return (dispatch) => {
    projectService
      .deleteProject(id)
      .then((success) => {
        dispatch(getAllProject());
        setTimeout(() => {
          openNotification({
            type: "success",
            message: "Delete project successfully!",
          });
        }, 300);
      })

      .catch((error) => {
        openNotification({
          type: "error",
          message: "Delete project fail!",
          description: error.response.data.message,
        });
      });
  };
};
