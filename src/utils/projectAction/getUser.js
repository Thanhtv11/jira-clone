import {
  setUser,
} from "../../redux/projectSlice/projectSlice";
import { projectService } from "../../services/project/project.service";
import { openNotification } from "../openNotification/openNotification";

export const getUser = () => {
  return (dispatch) => {
    projectService
      .getUser()
      .then((success) => {
        const user = success.data.content;
        dispatch(setUser(user));
      })

      .catch((error) => {
        openNotification({
          type: "error",
          message: "Get user of a project fail!",
          description: error.response.data.message,
        });
      });
  };
};
