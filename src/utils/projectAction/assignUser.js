import { projectService } from "../../services/project/project.service";
import {asisgnUserToProject} from "../../services/project/project.service"
import { openNotification } from "../openNotification/openNotification";
import { getAllProject } from "./getAllProject";

export const assignUser = (data) => {
  return (dispatch) => {
    projectService
      .asisgnUserToProject(data)
      .then((success) => {
        dispatch(getAllProject());
        openNotification({
          type: "success",
          message: "Asign User Successfully!",
        });
      })
      .catch((error) => {
        openNotification({
          type: "error",
          message: ` ${error.response.data.content}`,
        //   description: ` ${error.response.data.message}`,
        });
      });
  };
};
