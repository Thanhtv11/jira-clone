import { updateDetailModalStatus } from "../../redux/layoutSlice/layoutSlice";
import { projectService } from "../../services/project/project.service";
import { openNotification } from "../openNotification/openNotification";
import { getAllProject } from "./getAllProject";

export const updateProject = (id, data) => {
  return (dispatch) => {
    projectService
      .updateProject(id, data)
      .then((success) => {
        dispatch(updateDetailModalStatus(false));
        dispatch(getAllProject());
        openNotification({
          type: "success",
          message: "Update project details successfully!",
        });
      })
      .catch((error) => {
        openNotification({
          type: "error",
          message: "Update project details fail!!",
          description: ` ${error.response.data.creator}`,
        });
      });
  };
};
