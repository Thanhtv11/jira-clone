import { updateDetailModalStatus } from "../../redux/layoutSlice/layoutSlice";
import { setDetailOfProject } from "../../redux/projectSlice/projectSlice";
import { projectService } from "../../services/project/project.service";
import { openNotification } from "../openNotification/openNotification";

export const getDetailOfProject = (id) => {
  return (dispatch) => {
    projectService
      .getDetailOfProject(id)
      .then((success) => {
        const detailProject = success.data.content;
        dispatch(setDetailOfProject(detailProject));
        // dispatch(updateDetailModalStatus(true));
      })

      .catch((error) => {
        openNotification({
          type: "error",
          message: "Get project details fail!",
          description: error.response.data.message,
        });
      });
  };
};
