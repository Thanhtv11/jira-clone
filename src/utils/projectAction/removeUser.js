import { projectService } from "../../services/project/project.service";
import { openNotification } from "../openNotification/openNotification";
import { getAllProject } from "./getAllProject";

export const removeUser = (data) => {
  return (dispatch) => {
    projectService
      .removeUserFromProject(data)
      .then((success) => {
        dispatch(getAllProject());
        openNotification({
          type: "success",
          message: "Delete Successfully!",
        });
      })
      .catch((error) => {
        openNotification({
          type: "error",
          message: ` ${error.response.data.message}`,
        //   description: ` ${error.response.data.message}`,
        });
      });
  };
};
