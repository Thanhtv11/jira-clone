import { useDispatch } from "react-redux";
import { showModal } from "../../redux/layoutSlice/layoutSlice";

export const useShowModal = () => {
  const dispatch = useDispatch();
  const handleShowModal = (modalName) => {
    dispatch(showModal(modalName));
  };

  return handleShowModal;
};
