import React, { useEffect, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import CardProject from "../../Components/Utils/Card/CardProject";
import CardProjectCreateProject from "../../Components/Utils/Card/CardProjectCreateProject";
import CardProjectInfo from "../../Components/Utils/Card/CardProjectInfo";
import FormDetail from "../../Components/Utils/Form/FormDetail";
import SearchInput from "../../Components/Utils/Search/SearchInput";
import TagProjectCategory from "../../Components/Utils/Tag/TagProjectCategory";
import { getAllProject } from "../../utils/projectAction/getAllProject";
import { getProjectCategory } from "../../utils/projectAction/getProjectCategory";

export default function HomePage() {
  const { allProject } = useSelector((state) => state.projectSlice);
  const { arrSearchItem } = useSelector((state) => state.projectSlice);
  const { isSearch } = useSelector((state) => state.layoutSlice);
  const { detailOfProject } = useSelector((state) => state.projectSlice);
  const { isOpen } = useSelector((state) => state.layoutSlice);
  const dispatch = useDispatch();
  // console.log("allProject: ", allProject);

  useEffect(() => {
    dispatch(getAllProject());
    dispatch(getProjectCategory());
  }, [dispatch]);

  const projectInfoCards = useMemo(
    () =>
      !isSearch &&
      allProject &&
      allProject.map((project) => {
        const { id, categoryId, categoryName } = project;
        return (
          <CardProject
            key={id}
            CardContent={<CardProjectInfo projectInfo={project} />}
            cardTitle={
              <TagProjectCategory category={{ categoryId, categoryName }} />
            }
          />
        );
      }),
    [allProject, isSearch]
  );
  const filterdProjects = useMemo(
    () =>
      isSearch &&
      arrSearchItem &&
      arrSearchItem.map((project) => {
        const { id, categoryId, categoryName } = project;
        return (
          <CardProject
            key={id}
            CardContent={<CardProjectInfo projectInfo={project} />}
            cardTitle={
              <TagProjectCategory category={{ categoryId, categoryName }} />
            }
          />
        );
      }),
    [arrSearchItem, isSearch]
  );

  return (
    <>
      <SearchInput />
      <div
        className={`grid ${
          isOpen ? " bg-white bg-opacity-30" : ""
        } grid-cols-2 lg:grid-cols-4 gap-4 w-full h-full overflow-x-hidden`}
      >
        <CardProject
          CardContent={<CardProjectCreateProject />}
          cardTitle="Create new project"
        />
        {projectInfoCards}
        {filterdProjects}
      </div>
      {isOpen && Object.keys(detailOfProject).length > 0 && <FormDetail />}
    </>
  );
}
