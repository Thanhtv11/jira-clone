import { https } from "../configURL/configURL";

export const projectService = {
  getProjectGetAllProject: () => https.get("/api/Project/getAllProject"),

  getDetailOfProject: (idProject) =>
    https.get(`/api/Project/getProjectDetail?id=${idProject}`),

  getUser: () => https.get(`/api/Users/getUser`),
  asisgnUserToProject: (ProjectAndUserID) =>
    https.post(`/api/Project/assignUserProject`, ProjectAndUserID),

  getProjectCategory: () => https.get("/api/ProjectCategory"),

  postProjectCreateProject: (projectInfo) =>
    https.post("/api/Project/createProject", projectInfo),

  removeUserFromProject: (ProjectAndUserID) =>
    https.post(`/api/Project/removeUserFromProject`, ProjectAndUserID),

  deleteProject: (idProject) =>
    https.delete(`/api/Project/deleteProject?projectId=${idProject}`),

  updateProject: (idProject, data) =>
    https.put(`/api/Project/updateProject?projectId=${idProject}`, data),
};
