import axios from "axios";
import { userLocalStorage } from "../user/user.localStorage";
// const token = userLocalStorage.userInfo.get().accessToken;

const tokenCybersoft =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCAzMSIsIkhldEhhblN0cmluZyI6IjIzLzAyLzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY3NzExMDQwMDAwMCIsIm5iZiI6MTY0ODQwMDQwMCwiZXhwIjoxNjc3MjU4MDAwfQ.0byoDjBIIS6877xg7NwEnO16v5HOltI9AatD9OLB0Ys";

export const https = axios.create({
  baseURL: "https://jiranew.cybersoft.edu.vn",
  headers: {
    "Content-Type": "application/json",
    TokenCybersoft: tokenCybersoft,
    // Authorization: `Bearer ${token ? token : ""}`,
  },

  transformRequest: [
    function (data, headers) {
      const user = userLocalStorage.userInfo.get();
      const getAccessToken = () => (user ? user.accessToken : "");
      headers["Authorization"] = `Bearer ${getAccessToken()}`;
      return JSON.stringify(data);
    },
  ],
});
