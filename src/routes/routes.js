import LoginAndRegisterLayout from "../Components/HOCs/LoginAndRegisterLayout/LoginAndRegisterLayout";
import Sider from "../Components/HOCs/Sider/Sider";
import HomePage from "../Page/HomePage/HomePage";
import LoginPage from "../Page/LoginPage/LoginPage";
import RegisterPage from "../Page/RegisterPage/RegisterPage";

const LoginLayout = (Component) => (
  <LoginAndRegisterLayout Component={<Component />} />
);

const SiderLayout = (Component) => <Sider Component={<Component />} />;

export const routes = [
  {
    path: "/login",
    element: LoginLayout(LoginPage),
  },
  {
    path: "/register",
    element: LoginLayout(RegisterPage),
  },

  { path: "/", element: SiderLayout(HomePage) },
];
