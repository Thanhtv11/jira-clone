import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateDetailModalStatus } from "../../../redux/layoutSlice/layoutSlice";
import { removePrevDetail } from "../../../redux/projectSlice/projectSlice";
import { Select } from "antd";
import MyTinyEditor from "../MyTinyEditor/MyTinyEditor";
import { updateProject } from "../../../utils/projectAction/updateProject";
import { openNotification } from "../../../utils/openNotification/openNotification";

export default function FormDetail() {
  const { detailOfProject } = useSelector((state) => state.projectSlice);
  const {
    categoryName,
    projectName,
    description,
    categoryId,
    members,
    id,
    creator,
  } = detailOfProject;

  const { Option } = Select;
  const [input, setInput] = useState({
    id: "",
    projectName: "",
    creator: 0,
    description: "",
    categoryId: categoryId || "",
  });
  const handleChangeInput = (e) => {
    setInput({
      ...input,
      [e.target.name]: e.target.value,
    });
  };
  const handleUpdateProject = (id) => {
    if (!Object.values(input).every((i) => i !== "")) {
      openNotification({
        type: "error",
        message: "Please fill out the inputs!",
      });
    } else {
      dispatch(updateProject(id, input));
    }
  };
  const handleChangeSelect = (value) => {
    setInput({ ...input, categoryId: value });
  };
  const dispatch = useDispatch();
  const handleClickDetail = () => {
    dispatch(updateDetailModalStatus(false));
    dispatch(removePrevDetail());
  };
  return (
    <div
      role="alert"
      className="container bg-white pt-3 mx-auto w-11/12 md:w-2/3 max-w-lg transition duration-150 ease-in-out z-10 fixed top-0 right-0 bottom-0 left-0"
    >
      <div className="py-2 px-7 shadow-md h-full rounded border border-gray-400 flex flex-col space-y-5">
        <div>
          <p
            className={`text-gray-800 ${
              projectName.length > 20 ? "text-xl" : "text-2xl"
            } uppercase font-bold tracking-normal leading-tight `}
          >
            Name: {""}
            {projectName}
          </p>
          <input
            onChange={handleChangeInput}
            name="projectName"
            id="creator"
            className="text-gray-600 focus:outline-none focus:border focus:border-indigo-700 font-normal w-full h-10 flex items-center pl-3 text-sm border-gray-300 rounded border"
            placeholder={projectName}
          />
        </div>
        <div className="">
          <span className="text-gray-800 text-base font-bold tracking-normal leading-tight">
            {" "}
            Creator: {""}
          </span>
          <span className="uppercase font-semibold">{creator.name}</span>
        </div>
        <div className="">
          <span className="text-gray-800 text-base font-bold tracking-normal leading-tight">
            Members: {""}
          </span>

          {members?.length > 0 ? (
            members.map((m, i) => (
              <span key={m.userId} className="uppercase ml-1 font-semibold">
                {members.length - (i + 1) === 0 ? `${m.name}` : `  ${m.name},`}
              </span>
            ))
          ) : (
            <span className="uppercase text-base font-semibold">
              Creator Only
            </span>
          )}
        </div>
        <div className=" flex justify-between items-center">
          <div className="">
            <p className="text-gray-800 text-base font-bold tracking-normal leading-tight ">
              {" "}
              Category:{" "}
              <span className="uppercase font-semibold ">{categoryName}</span>
            </p>
            <Select
              className="uppercase"
              defaultValue={categoryName}
              style={{
                width: 180,
              }}
              onChange={handleChangeSelect}
            >
              <Option className="uppercase" value="1">
                Dự Án Web
              </Option>
              <Option className="uppercase" value="2">
                Dự Án Phần Mềm
              </Option>
              <Option className="uppercase" value="3">
                Dự Án Di Động
              </Option>
            </Select>
          </div>
          <div className="">
            <p className="text-gray-800 text-base font-bold tracking-normal leading-tight">
              {" "}
              ID: <span className="uppercase font-semibold">{id}</span>
            </p>
            <input
              onChange={handleChangeInput}
              name="id"
              id="id"
              className="text-gray-600 focus:outline-none focus:border focus:border-indigo-700 font-normal w-full h-10 flex items-center pl-3 text-sm border-gray-300 rounded border"
              placeholder={id}
            />
          </div>
        </div>
        <div className="">
          <p className="text-gray-800 text-xl font-bold tracking-normal leading-tight ">
            Description
          </p>
          <textarea
            onChange={handleChangeInput}
            name="description"
            id="description"
            className="text-gray-600 focus:outline-none focus:border focus:border-indigo-700 font-normal w-full h-28 flex items-center pl-3 text-sm border-gray-300 rounded border"
            placeholder={`${description.replace(/<(.|\n)*?>/g, "")}...`}
          />

          {/* <MyTinyEditor
            placeholder={`${description.replace(/<(.|\n)*?>/g, "")}...`}
            editorRef={``}
          /> */}
        </div>
        <div className="flex  items-center justify-start w-full">
          <button
            onClick={() => handleUpdateProject(id)}
            className="focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-700 transition duration-150 ease-in-out hover:bg-indigo-600 bg-indigo-700 rounded text-white px-8 py-2 text-sm"
          >
            Update
          </button>
          <button
            className="focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-400 ml-3 bg-gray-100 transition duration-150 text-gray-600 ease-in-out hover:border-gray-400 hover:bg-gray-300 border rounded px-8 py-2 text-sm"
            onClick={() => handleClickDetail()}
          >
            Cancel
          </button>
        </div>
        <button
          className="cursor-pointer absolute  top-0 right-0 mt-4 mr-5 text-gray-400 hover:text-gray-600 transition duration-150 ease-in-out rounded focus:ring-2 focus:outline-none focus:ring-gray-600"
          onClick={() => handleClickDetail()}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="icon icon-tabler icon-tabler-x"
            width={30}
            height={30}
            viewBox="0 0 24 24"
            strokeWidth="2.5"
            stroke="currentColor"
            fill="none"
            strokeLinecap="round"
            strokeLinejoin="round"
          >
            <path stroke="none" d="M0 0h24v24H0z" />
            <line x1={18} y1={6} x2={6} y2={18} />
            <line x1={6} y1={6} x2={18} y2={18} />
          </svg>
        </button>
      </div>
    </div>
  );
}
