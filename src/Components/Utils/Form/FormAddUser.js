import { Select } from "antd";
import React, { useEffect } from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { openNotification } from "../../../utils/openNotification/openNotification";
import { assignUser } from "../../../utils/projectAction/assignUser";
import { getUser } from "../../../utils/projectAction/getUser";

export default function FormAddUser({ id, members }) {
  const dispatch = useDispatch();
  const { Option } = Select;
  const { User } = useSelector((state) => state.projectSlice);
  const [objParams, setObjParams] = useState({
    projectId: id,
    userId: "",
  });
  const handleClickAdd = (id) => {
    console.log(id);
  };

  useEffect(() => {
    if (objParams.userId) {
      dispatch(assignUser(objParams));
    }
  }, [objParams.userId]);

  useEffect(() => {
    dispatch(getUser());
  }, []);
  // console.log(arrMembers);
  const handleChange = (value) => {
    const id = value[value.length - 1];

    const check = members.map(({ userId }) => userId === id);
    const isHas = check.every((i) => i === false);
    if (!isHas) {
      // openNotification({
      //   type: "error",
      //   message: `This Member is already in the team`,
      // });
      return;
    }
    setObjParams({ ...objParams, userId: id || "" });
  };
  return (
    <div className="absolute w-44 top-5 -right-4">
      {/* <button
        onClick={handleSubmitAssign}
        className="px-4 py-2 w-20 text-base uppercase font-semibold"
      >
        Close
      </button> */}
      <Select
        className="overflow-y-scroll"
        mode="multiple"
        style={{
          width: "100%",
        }}
        placeholder="Click To Find Members"
        onChange={handleChange}
        optionLabelProp="label"
      >
        {User?.length > 0 &&
          User?.slice(0, 50).map((u) => (
            <Option key={u.userId} value={u.userId} label={u.name}>
              <span
                onClick={() => handleClickAdd(u.userId)}
                className="text-sm"
              >
                {u.name}
              </span>
            </Option>
          ))}
      </Select>
    </div>
  );
}
