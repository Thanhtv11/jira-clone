import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { removeUser } from "../../../utils/projectAction/removeUser";

export default function FormLoadMembers({ id, members, setIsToggleMember }) {
  const dispatch = useDispatch();
  const [objParams, setObjParams] = useState({
    projectId: id,
    userId: "",
  });
  const handleDeleteUser = async (userId) => {
    setObjParams({ ...objParams, userId });
  };

  useEffect(() => {
    if (objParams.userId !== "") {
      dispatch(removeUser(objParams));
    }
  }, [objParams.userId]);
  const handleClickDone = () => {
    setIsToggleMember(false);
  };

  return (
    <div className="absolute top-10  z-50 flex flex-col bg-black">
      <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div className=" inline-block min-w-full sm:px-4 lg:px-8">
          <div className="overflow-hidden">
            <table className=" text-center">
              <thead className="border-b">
                <tr className="border-b bg-green-100 border-green-200">
                  <th
                    scope="col"
                    className="text-md font-semibold text-gray-900 px-4 py-4"
                  >
                    ID
                  </th>
                  <th
                    scope="col"
                    className="text-md font-semibold text-gray-900 px-4 py-4"
                  >
                    Avatar
                  </th>
                  <th
                    scope="col"
                    className="text-md font-semibold text-gray-900 px-4 py-4"
                  >
                    Name
                  </th>
                </tr>
              </thead>
              <tbody>
                {members?.length > 0 ? (
                  members?.map((m, i) => (
                    <tr key={i} className="border-b">
                      <td className="text-sm text-white font-medium px-4 py-4 whitespace-nowrap">
                        {m.userId}
                      </td>
                      <td className="text-sm text-white font-light px-4 py-4 whitespace-nowrap">
                        <img
                          className="w-6 h-6 object-cover rounded-full"
                          src={m.avatar}
                          alt=""
                        ></img>
                      </td>
                      <td className="text-sm text-white font-light px-4 py-4 whitespace-nowrap flex space-x-2">
                        <span>{m.name}</span>
                        <img
                          onClick={() => handleDeleteUser(m.userId)}
                          className="w-6 h-6 cursor-pointer hover:brightness-75"
                          src=" https://cdn-icons-png.flaticon.com/512/1828/1828851.png"
                          alt=""
                        ></img>
                      </td>
                    </tr>
                  ))
                ) : (
                  <tr className="border-b">
                    <td className="text-sm text-white font-medium px-4 py-4 whitespace-nowrap">
                      None
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
          <button
            onClick={handleClickDone}
            type="button"
            className="flex  mx-auto self-center bg-teal-100 px-4 py-2 text-md font-semibold text-teal-500 transition duration-300 hover:bg-teal-200"
          >
            Done
          </button>
        </div>
      </div>
    </div>
  );
}
