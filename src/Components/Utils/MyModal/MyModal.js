import React, { useMemo } from "react";
import { Modal } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { closeModal } from "../../../redux/layoutSlice/layoutSlice";
import { formCreateProject, formCreateTask } from "./myModalName";
import FormCreateTask from "../Form/FormCreateTask";
import FormCreateProject from "../Form/FormCreateProject";

export default function MyModal() {
  const { modal } = useSelector((state) => state.layoutSlice);
  const { modalName, modalState } = modal;
  const dispatch = useDispatch();

  const handleCancel = () => {
    dispatch(closeModal());
  };

  const modalContent = useMemo(() => {
    switch (modalName) {
      case formCreateTask:
        return { title: "Create task", content: <FormCreateTask /> };

      case formCreateProject:
        return { title: "Create project", content: <FormCreateProject /> };

      default:
        return { title: "Modal", content: <></> };
    }
  }, [modalName]);

  const { title, content } = modalContent;

  return (
    <>
      <Modal title={title} open={modalState} onCancel={handleCancel} footer={null}>
        {content}
      </Modal>
    </>
  );
}
