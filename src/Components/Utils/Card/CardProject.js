import React from "react";

const cardStyle = { minWidth: 220 };

export default function CardProject({ CardContent, cardTitle }) {
  return (
    <>
      <div
        style={cardStyle}
        className="w-full h-48 flex flex-col justify-between pb-2 rounded bg-white"
      >
       {CardContent}
        <span className="h-9 m-0 px-4 ml-auto whitespace-nowrap text-right">{cardTitle}</span>
      </div>
    </>
  );
}
