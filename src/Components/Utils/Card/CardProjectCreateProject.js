import React from "react";
import { PlusCircleOutlined } from "@ant-design/icons";
import { useShowModal } from "../../../utils/layoutAction/useShowModal";
import { formCreateProject } from "../MyModal/myModalName";
import { useDispatch, useSelector } from "react-redux";
import { updateDetailModalStatus } from "../../../redux/layoutSlice/layoutSlice";
import { removePrevDetail } from "../../../redux/projectSlice/projectSlice";

export default function CardProjectCreateProject() {
  const showModal = useShowModal();
  const dispatch = useDispatch();
  const { isOpen } = useSelector((state) => state.layoutSlice);
  const handleShowModal = () => {
    if (isOpen) {
      dispatch(removePrevDetail());
      dispatch(updateDetailModalStatus(false));
    }
    showModal(formCreateProject);
  };

  return (
    <>
      <div
        className="h-full flex justify-center items-center border-b-2 cursor-pointer transition-all duration-300 hover:border-blue-300 hover:bg-blue-300 hover:text-white"
        onClick={handleShowModal}
      >
        <PlusCircleOutlined className="text-4xl" />
      </div>
    </>
  );
}
