import React, { useMemo } from "react";
import { Menu } from "antd";
import {
  SearchOutlined,
  PlusOutlined,
  HomeOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { buttonKey, getItem } from "./myMenuUtils";
import { useNavigate, useParams } from "react-router-dom";
import { formCreateTask } from "../MyModal/myModalName";
import { useShowModal } from "../../../utils/layoutAction/useShowModal";
import { useLogout } from "../../../utils/userAction/useLogout";
import { useDispatch } from "react-redux";
import {
  updateDetailModalStatus,
  updateSearchStatus,
} from "../../../redux/layoutSlice/layoutSlice";

const { linkToHomePage, openCreateTaskModal, openSearchTaskModal, logout } =
  buttonKey;

export default function MyMenu({ items }) {
  const history = useNavigate();
  const dispatch = useDispatch();
  const { projectID } = useParams();
  const handleShowModal = useShowModal();
  const handleLogout = useLogout();

  const upperMenuItems = useMemo(
    () => [
      getItem("HOME", linkToHomePage, <HomeOutlined />, false),
      getItem("SEARCH TASK", openSearchTaskModal, <SearchOutlined />, false),
      getItem(
        "CREATE TASK",
        openCreateTaskModal,
        <PlusOutlined />,
        !projectID ? false : true
      ),
    ],
    [projectID]
  );
  const belowMenuItems = useMemo(
    () => [getItem("LOGOUT", logout, <LogoutOutlined />, false)],
    []
  );

  const menuItemClicked = ({ key }) => {
    switch (key) {
      case linkToHomePage:
        dispatch(updateSearchStatus(false));
        return history("/");

      case openSearchTaskModal:
        return handleShowModal("");

      case openCreateTaskModal:
        return handleShowModal(formCreateTask);

      case logout:
        return handleLogout();

      default:
        return () => {};
    }
  };

  const menuItems = useMemo(() => {
    switch (items) {
      case "upperMenuItems":
        return upperMenuItems;

      case "belowMenuItems":
        return belowMenuItems;

      default:
        return [];
    }
  }, [items, upperMenuItems, belowMenuItems]);

  return (
    <>
      <Menu
        theme="dark"
        selectedKeys={["0"]}
        mode="inline"
        items={menuItems}
        onClick={menuItemClicked}
      />
    </>
  );
}
