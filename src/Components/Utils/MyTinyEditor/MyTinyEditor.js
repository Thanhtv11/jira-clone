import { Editor } from "@tinymce/tinymce-react";
import React from "react";

export default function MyTinyEditor({ editorRef, placeholder }) {
  return (
    <>
      <Editor
        apiKey="92r0k4njmo36r43csddiwwj59yu9x99fub71i90f8z0y2wsq"
        onInit={(evt, editor) => (editorRef.current = editor)}
        initialValue={`${placeholder}`}
        init={{
          height: 200,
          menubar: false,

          // plugins:
          //   "powerpaste casechange importcss tinydrive searchreplace directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap pagebreak nonbreaking anchor insertdatetime advlist lists checklist wordcount tinymcespellchecker a11ychecker help formatpainter permanentpen pageembed charmap mentions quickbars linkchecker emoticons advtable export",

          toolbar:
            "undo redo | " +
            "bold italic underline " +
            "forecolor backcolor emoticons charmap | " +
            "bullist numlist checklist outdent indent  " +
            "alignleft aligncenter alignright alignjustify  " +
            "removeformat codesample   ",

          quickbars_selection_toolbar: "blockquote quicklink",

          content_style:
            "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }",
        }}
      />
    </>
  );
}
