import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useSearchParams } from "react-router-dom";
import { updateSearchStatus } from "../../../redux/layoutSlice/layoutSlice";
import {
  setAllProject,
  setSearchProject,
} from "../../../redux/projectSlice/projectSlice";
import { openNotification } from "../../../utils/openNotification/openNotification";

export default function SearchInput() {
  const dispatch = useDispatch();
  const [searchInput, setSearchInput] = useState("");
  const { allProject } = useSelector((state) => state.projectSlice);
  const [searchParams, setSearchParams] = useSearchParams();
  const getQuery = searchParams.get("query");

  const handleChangeInput = (e) => {
    setSearchInput(e.target.value.toUpperCase());
    // setSearchParams({ query: e.target.value.toUpperCase() });
  };

  useEffect(() => {
    if (getQuery) {
      // const filteredProject = allProject?.filter((p) =>
      //   p.projectName.toUpperCase().includes(getQuery)
      // );
      // dispatch(setSearchProject(filteredProject));
      // dispatch(updateSearchStatus(true));
    }
  }, [getQuery]);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!searchInput) {
      dispatch(setSearchProject([]));
      dispatch(updateSearchStatus(false));
      //   openNotification({
      //     type: "error",
      //     message: "Please enter the input",
      //   });
      // return;
    } else {
      const filteredProject = allProject.filter((p) =>
        p.projectName.toUpperCase().includes(searchInput)
      );
      dispatch(setSearchProject(filteredProject));
      dispatch(updateSearchStatus(true));
    }
  };
  return (
    <div className="relative mb-4">
      <form onSubmit={handleSubmit} className="flex items-center">
        <svg
          className="w-5 h-5 text-gray-500 dark:text-gray-400 absolute ml-2"
          fill="currentColor"
          viewBox="0 0 20 20"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
            clipRule={`evenodd`}
          ></path>
        </svg>
        <input
          onChange={(e) => handleChangeInput(e)}
          type="text"
          id="table-search"
          className="bg-white text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-80 pl-10 p-2.5 dark:border-gray-600 "
          placeholder="Search for items"
        />
      </form>
    </div>
  );
}
