import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  allProject: null,
  projectCategory: null,
  User: [],
  detailOfProject: {},
  arrSearchItem: [],
};

const projectSlice = createSlice({
  name: "projectSlice",
  initialState,
  reducers: {
    setAllProject: (state, { payload }) => {
      state.allProject = payload;
    },
    setProjectCategory: (state, { payload }) => {
      state.projectCategory = payload;
    },
    setUser: (state, { payload }) => {
      state.User = payload;
    },
    setDetailOfProject: (state, { payload }) => {
      state.detailOfProject = payload;
    },
    removePrevDetail: (state) => {
      state.detailOfProject = {};
    },
    setSearchProject: (state, { payload }) => {
      state.arrSearchItem = payload;
    },
  },
});

export const {
  setAllProject,
  setProjectCategory,
  setUser,
  setDetailOfProject,
  removePrevDetail,
  setSearchProject,
} = projectSlice.actions;

export default projectSlice.reducer;
