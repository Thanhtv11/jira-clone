import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  modal: { modalState: false, modalName: "" },
  isOpen: false,
  isSearch: false,
};

const layoutSlice = createSlice({
  name: "layoutSlice",
  initialState,
  reducers: {
    showModal: (state, { payload }) => {
      state.modal = {
        modalState: true,
        modalName: payload,
      };
    },
    closeModal: (state) => {
      state.modal = {
        modalState: false,
        modalName: "",
      };
    },
    updateDetailModalStatus: (state, { payload }) => {
      state.isOpen = payload;
    },
    updateSearchStatus: (state, { payload }) => {
      state.isSearch = payload;
    },
  },
});

export const { showModal, closeModal, updateDetailModalStatus,updateSearchStatus } =
  layoutSlice.actions;

export default layoutSlice.reducer;
